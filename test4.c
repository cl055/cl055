#include <stdio.h>
int sum(int);
int main()
{
	int n;
	printf("Enter the number\n");
	scanf("%d",&n);
	printf("The sum of digits= %d\n", sum(n));
	return 0;
}

int sum(int n)
{
	int sum=0,rem;
	while (n!=0)
	{
		rem=n%10;
		sum=rem+sum;
		n=n/10;
	}
	return (sum);
}

